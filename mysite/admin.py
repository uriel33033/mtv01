from django.contrib import admin
from .models import NewTable,Product #models.py的新增TABLE
# Register your models here.

admin.site.register(NewTable) #註冊讓管理者能夠使用這個table

class ProductAdmin(admin.ModelAdmin):
    list_display = ('name','price','qty')  #設定顯示出的樣式

admin.site.register(Product,ProductAdmin)